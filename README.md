# DotnetDev.Homework.13

Рефлексия и её применение

ДЗ - консольное приложение Reflect. 

В нем реализован класс Serialisations с методами сериализации и десериализации.

Реализованы методы для ДЗ:
1.	BasicSerialisation –сериализация через Binary formatter в .net 
2.	BasicDeserialisation – десериализация п.1
3.	MySerialisation – моя упаковка в формат типа JSON, добавляется также тип поля (для дз избыточно). Для наглядности записываю не значения из конструктора F, а умножаю их на 2
4.	MyDeserialisation – десериализация п.3 в класс F из ДЗ

Опреации были в цикле по 10тыс раз
BinaryFormatter:
BasicSerialisation: 2,2343277 с
BasicDeserialisation: 0,5138988с 
The diff is: -1,7202794000000001 с

MySerialisation
MySerialisation: 1,6958284
MyDeSerialisation: 0,1609595
The diff is: 1,5348689

NewtonSoft
NewtonSoft:{"i1":2,"i2":4,"i3":6,"i4":8,"i5":10}
NewtonSoft serialisation: 0,1031466
newtonsoft DeserializeObject: i1:2 i2:4 i3:6 i4:8 i5:10
NewtonSoft deserialize:0,0609194
The diff is: 0,042227200000000006

