var builder = WebApplication.CreateBuilder(args);
var app = builder.Build();

app.MapGet("/", () => "Hello World!");

static void StringType()
{
    var hello = "hello";
    var length = hello.Length;

    Console.WriteLine(hello);
    Console.WriteLine(length);
}


StringType();

app.Run();
