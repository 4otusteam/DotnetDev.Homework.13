﻿using System.Runtime.Serialization.Formatters.Binary;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Reflection;

namespace Reflect
{
    public class Serialisations
    {


        public Stopwatch BasicSerialisation(F res)
        {
            var bf = new BinaryFormatter();
            Stopwatch watch =  Stopwatch.StartNew();
            for (int i = 0; i < 10000; i++)
            {
                using (var fs = new FileStream(@"D:\Projects\DotnetDev.Homework.13\Reflect\fs.bin", FileMode.Create))
                {
                    bf.Serialize(fs, res);
                }
            }
            watch.Stop();
            return watch;
        }


        public Stopwatch BasicDeserialisation(F res)
        {
            var bf = new BinaryFormatter();

            Stopwatch watch = System.Diagnostics.Stopwatch.StartNew();

            for (int i = 0; i < 10000; i++)
            {
                using (var fs = new FileStream(@"D:\Projects\DotnetDev.Homework.13\Reflect\fs.bin", FileMode.Open))
                {
                    var after = (F)bf.Deserialize(fs);
                }
            }
            var elapsedMs2 = watch.ElapsedMilliseconds;
            return watch;


        }

        

        public Stopwatch MySerialisation(Object f)
        {
            var properties = f.GetType().GetFields();

            Stopwatch watch = System.Diagnostics.Stopwatch.StartNew();
            for (int i = 0; i < 10000; i++)
            {

                String class2String = String.Empty;
                foreach (var property in properties)
                {
                    class2String += String.Format("{{{0}:{{{1}:{2}}}}}," + System.Environment.NewLine, property.Name, property.GetValue(f), property.FieldType);
                }
                //Removing final comma
                class2String = class2String.Substring(0, class2String.Length - 1);

                WriteToSerFile(class2String);
            }
            watch.Stop();
            return watch;
        }

        // T  - must be a class and must support creating instances
        public T MyDeSerialize<T>(string str2Class) where T : class, new()
        {
            Regex regex = new Regex("{(?<fieldName>\\w*):{(?<fieldValue>\\w*):(?<fieldType>\\w*.\\w*)}}", RegexOptions.Multiline);
            
            MatchCollection s = regex.Matches(str2Class);

            var res = new T();
            var fields = typeof(T).GetFields(BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty | BindingFlags.SetProperty);


            foreach (var field in fields)
            {
                foreach (Match match in s)
                {
                    string newType = match.Groups["fieldType"].Value;
                    string fieldName = match.Groups["fieldName"].Value;
                    var fieldValue = match.Groups["fieldValue"].Value;

                    if (fieldName == field.Name)
                    {
                        field.SetValue (res, Convert.ChangeType(fieldValue, field.FieldType));
                    }
                }
            }
            return res;

        }



        public Stopwatch ExecMyDeserialise()
        {
            Stopwatch watch = System.Diagnostics.Stopwatch.StartNew();

            String class2String = File.ReadAllText(@"D:\Projects\DotnetDev.Homework.13\Reflect\myserialisation.txt");
            for (int i = 0; i < 10000; i++)
            {
                MyDeSerialize<B1>(class2String);
            }
            watch.Stop();
            return watch;
        }

            private Boolean WriteToSerFile(string class2String)
        {
            using (var fs = new FileStream(@"D:\Projects\DotnetDev.Homework.13\Reflect\myserialisation.txt", FileMode.Create))
            {
                byte[] bytes = Encoding.UTF8.GetBytes(class2String);
                fs.Write(bytes, 0, bytes.Length);
                return true;
            }
            return false;
        }


    }
}
