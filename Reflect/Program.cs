﻿// See https://aka.ms/new-console-template for more information
using System.Diagnostics;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Reflect;

namespace Reflect;



[Serializable]
public class F
{
    public int i1, i2, i3, i4, i5;
    public F ()
    {
        i1 = 1;
        i2 = 2;
        i3 = 3;
        i4 = 4;
        i5 = 5;
    }
    public override string ToString()
    {
        return $"i1:{i1} i2:{i2} i3:{i3} i4:{i4} i5:{i5}";
    }

}


public class B1
{
    public int i1, i2, i3;
    public string s1, s2;
    public B1()
    {
        i1 = 5; i2 = 10; i3 = 11;
        s1 = "String 1"; s2 = "String 22";
    }
    public override string ToString()
    {
        return $"i1:{i1};  i2:{i2}; i3:{i3}; s1:{s1}; s2:{s2}";
    }
}


class Program
{

    public static Boolean ObjCompare(object v1, object v2)
    {
        var properties = v1.GetType().GetProperties();

        foreach(var property in properties)
        {
            Console.WriteLine(property.PropertyType); 
            Console.WriteLine(property.GetValue(v1)); 
        }
        return true;

    }


    public static void Main()
    {
        //F f = Activator.CreateInstance<F>();

        Serialisations serialisations = new Serialisations();

        F f = new F();

        //My Serialisation. Format is 
        //{ i1: { 1:System.Int32} } ,{ i2: { 2:System.Int32} } ,{ i3: { 3:System.Int32} } ,{ i4: { 4:System.Int32} } ,{ i5: { 5:System.Int32} }

        F newf = new F();



        B1 newB = new B1();
        Stopwatch basicSer = serialisations.BasicSerialisation(f);
        Console.WriteLine($"BasicSerialisation: {basicSer.Elapsed.TotalSeconds}");
        Stopwatch basicDeser = serialisations.BasicDeserialisation(f);
        Console.WriteLine($"BasicDeserialisation: {basicDeser.Elapsed.TotalSeconds}");

        Console.WriteLine($"The diff is: " + (basicDeser.Elapsed.TotalSeconds - basicSer.Elapsed.TotalSeconds));
        Console.WriteLine("--------------");


        Stopwatch myser = serialisations.MySerialisation(newB);
        Console.WriteLine($"MySerialisation: {myser.Elapsed.TotalSeconds}");


        
        //My desialisation
        
        Stopwatch mydeser = serialisations.ExecMyDeserialise();
        Console.WriteLine("MyDeserialisation:" + mydeser.Elapsed.TotalSeconds);
        Console.WriteLine("The diff is: " + (myser.Elapsed.TotalSeconds - mydeser.Elapsed.TotalSeconds));

        //Это я переписываю
        //Stopwatch mydeser = serialisations.MyDeserialisation(newB);
        //Console.WriteLine($"MyDeSerialisation: {mydeser.Elapsed.TotalSeconds}");

        Console.WriteLine("--------------");

        Stopwatch jsonwatch = Stopwatch.StartNew();
        String jsonOut = String.Empty;
        for (int i = 0; i < 1; i++)
        {
            var nsOut = JsonConvert.SerializeObject(f);
            jsonOut = nsOut;
        }
        jsonwatch.Stop();
        Console.WriteLine("NewtonSoft:" + jsonOut);
        Console.WriteLine("NewtonSoft:" + jsonwatch.Elapsed.TotalSeconds);



        Stopwatch dejsonwatch = Stopwatch.StartNew();
        for (int i = 0; i < 10000; i++)
        {
            F desNs = JsonConvert.DeserializeObject<F>(jsonOut);
            if(i == 10000-1) Console.WriteLine("newtonsoft DeserializeObject: " + desNs);

        }
        dejsonwatch.Stop();
        Console.WriteLine("NewtonSoft deserialize:" + dejsonwatch.Elapsed.TotalSeconds);
        Console.WriteLine($"The diff is: " + (dejsonwatch.Elapsed.TotalSeconds - jsonwatch.Elapsed.TotalSeconds));




    }
}

